import com.devcamp.javabasic_j01.s10.Account;
import com.devcamp.javabasic_j01.s10.Customer;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        Customer customer1 = new Customer();
        Customer customer2 = new Customer(1, "Hong", 20);
        System.out.println("Customer1");
        System.out.println(customer1);
        System.out.println("Customer2");
        System.out.println(customer2);

        Account account1 = new Account(11, customer1, 20);
        Account account2 = new Account(21, customer2, 40);

        System.out.println(account1);
        System.out.println(account2);

    }
}
