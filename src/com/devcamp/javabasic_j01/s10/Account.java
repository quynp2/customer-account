package com.devcamp.javabasic_j01.s10;

import javax.swing.SortingFocusTraversalPolicy;

public class Account {
    private int id;
    private Customer customer;
    private double balance = 0.0;

    public Account(int id, Customer customer, double balance) {
        this.id = id;
        this.customer = customer;
        this.balance = balance;
    }

    public Account(int id, Customer customer) {
        this.id = id;
        this.customer = customer;
    }

    public int getId() {
        return id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        double roundOff = (double) Math.round(balance * 100) / 100;
        return "Account [id=" + id + ", customer=" + customer + ", balance= $" + roundOff + "]";
    }

    public String getCustomerName() {
        return customer.getName();
    }

    public Account deposit(double amount) {
        this.balance += amount;
        return this;
    }

    public Account withdraw(double amount) {
        if (this.balance >= amount) {
            this.balance -= amount;

        } else {
            System.out.println("Amount withdraw exceeded the curent balance");
        }
        return this;
    }

}
